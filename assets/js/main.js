// Menu fixed
$(window).scroll(function() {
    if ($(this).scrollTop() > 250) {
        $('#header').addClass("fix-nav");
        $('body').addClass("fix-body")
    } else {
        $('#header').removeClass("fix-nav");
        $('body').removeClass("fix-body")
    }
});


//Search
$(document).ready(function() {
    $(".search-active").on('click', function(event) {
        event.preventDefault();
        $(this).parent().find('.account-dropdown, .search-content').slideToggle('medium');
    });
});


// Owl Carousel
$('.owl-carousel').owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 1
        },
        1000: {
            items: 1
        }
    }
})


$('#myCarousel').carousel({
    interval: false
});
$('#carousel-thumbs').carousel({
    interval: false
});

$('#myCarousel_1').carousel({
    interval: false
});
$('#carousel-thumbs-1').carousel({
    interval: false
});



// Back to top

$(document).ready(function() {
    $('.back-top').append('<div id="toTop"><i class="fas fa-chevron-up"></i></div>');
    $(window).scroll(function() {
        if ($(this).scrollTop() != 0) {
            $('#toTop').fadeIn();
        } else {
            $('#toTop').fadeOut();
        }
    });
    $('#toTop').click(function() {
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });
});






// Modal Show 
$(window).on('load', function() {
    setTimeout(function() {
        $('#myModal').modal('show')
    }, 4500);
});


$(document).ready(function() {
    $(".btn_form").click(function() {
        alert("ĐANG UPDATE CHỨC NĂNG");
    });
});



$(".fa-bars").click(function() {
    $(".ul").slideToggle();
});


$(".menu").click(
    function() {
        $('.menu .menu-top').toggleClass('menu-top-click');
        $('.menu .menu-middle').toggleClass('menu-middle-click');
        $('.menu .menu-bottom').toggleClass('menu-bottom-click');
    }
);